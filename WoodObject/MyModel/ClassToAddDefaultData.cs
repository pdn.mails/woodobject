﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyModel
{
    /// <summary>
    /// Class generating default data to use methods MyModel class.
    /// </summary>
    public class ClassToAddDefaultData
    {
        private const int maxObjects = 17;

        /// <summary>
        /// Constructor of ClassToAddDefaultData.
        /// </summary>
        public ClassToAddDefaultData()
        {

        }

        /// <summary>
        /// Method to get list default names of "Type".
        /// </summary>
        /// <returns>List names "Type".</returns>
        public List<string> GetTypes()
        {
            // populate the type
            List<string> types = new List<string>()
            {
                new string("Good".ToCharArray()),
                new string("Bad".ToCharArray()) 
            };
            return types;
        }

        /// <summary>
        /// Method to get list default names of "Status".
        /// </summary>
        /// <returns>List names "Status".</returns>
        public List<string> GetStatuses()
        {
            // populate the status
            List<string> statuses = new List<string>()
            {
                new string("OnLine".ToCharArray()),
                new string("N/A".ToCharArray()),
                new string("OffLine".ToCharArray())
            };
            return statuses;
        }
        /// <summary>
        /// Method to get list defaults properties of "Object".
        /// Properties with defaults name "Object", "Type" and "Status".
        /// </summary>
        /// <returns>List string[] of names: [0] is name "Object",
        /// [1] is name "Type",
        /// [2] is name "Status".
        /// </returns>
        public List<string[]> GetObjects()
        {
            List<string> types = GetTypes();
            List<string> statuses = GetStatuses();

            // populate the object
            List<string[]> objects = new List<string[]>();

            for (int i = 0; i < maxObjects; i++)
            {
                string[] temp = new string[3];

                temp[0] = i + "_Name";
                temp[1] = types[i % types.Count];
                temp[2] = statuses[i % statuses.Count];

                objects.Add(temp);
            }
            return objects;
        }

        /// <summary>
        /// Method to get default properties of "Relate".
        /// Properties with defaults name child "Object" and parent "Object".
        /// </summary>
        /// <returns>List string[] of names: [0] is name child "Object",
        /// [1] is name parent "Object".
        /// </returns>
        public List<string[]> GetRelates()
        {
            List<string[]> objects = GetObjects();

            // populate the relate
            List<string[]> relates = new List<string[]>();

            relates.Add(new string[] { objects[0].GetValue(0).ToString(), String.Empty });
            relates.Add(new string[] { objects[1].GetValue(0).ToString(), String.Empty });
            relates.Add(new string[] { objects[2].GetValue(0).ToString(), objects[0].GetValue(0).ToString() });
            relates.Add(new string[] { objects[3].GetValue(0).ToString(), objects[0].GetValue(0).ToString() });
            relates.Add(new string[] { objects[4].GetValue(0).ToString(), String.Empty });
            relates.Add(new string[] { objects[5].GetValue(0).ToString(), objects[3].GetValue(0).ToString() });
            relates.Add(new string[] { objects[6].GetValue(0).ToString(), String.Empty });
            relates.Add(new string[] { objects[7].GetValue(0).ToString(), objects[6].GetValue(0).ToString() });
            relates.Add(new string[] { objects[8].GetValue(0).ToString(), objects[6].GetValue(0).ToString() });
            relates.Add(new string[] { objects[9].GetValue(0).ToString(), objects[8].GetValue(0).ToString() });
            relates.Add(new string[] { objects[10].GetValue(0).ToString(), objects[7].GetValue(0).ToString() });
            relates.Add(new string[] { objects[11].GetValue(0).ToString(), objects[7].GetValue(0).ToString() });
            relates.Add(new string[] { objects[12].GetValue(0).ToString(), objects[7].GetValue(0).ToString() });
            relates.Add(new string[] { objects[13].GetValue(0).ToString(), objects[10].GetValue(0).ToString() });
            relates.Add(new string[] { objects[14].GetValue(0).ToString(), objects[4].GetValue(0).ToString() });
            relates.Add(new string[] { objects[15].GetValue(0).ToString(), objects[4].GetValue(0).ToString() });
            relates.Add(new string[] { objects[16].GetValue(0).ToString(), objects[4].GetValue(0).ToString() });

            return relates;
        }
    }
}
