﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EF;
using Core;
using System.Collections.ObjectModel;

namespace MyModel
{
    /// <summary>
    /// Model class.
    /// </summary>
    public class MyModel
    {
        private readonly AppDbContext dbContext;
        private readonly ObservableCollection<MyTreeItem> myTrees;
        public ObservableCollection<MyTreeItem> MyTrees => myTrees;

        /// <summary>
        /// Constructor of MyModel.
        /// </summary>
        public MyModel()
        {
            dbContext = new AppDbContext();
            myTrees = new ObservableCollection<MyTreeItem>();
        }

        //-----------------------------------------------------------------------------

        /// <summary>
        /// Method to add default data in Model.
        /// </summary>
        public void AddDefaultData()
        {
            ClassToAddDefaultData data = new ClassToAddDefaultData();

            foreach (string name in data.GetTypes())
                this.AddType(name);

            foreach (string name in data.GetStatuses())
                this.AddStatus(name);

            foreach (string[] names in data.GetObjects())
                this.AddObject(names[0], names[1], names[2]);

            foreach (string[] names in data.GetRelates())
                this.AddRelate(names[0], names[1]);
        }

        //-----------------------------------------------------------------------------

        #region Objects

        /// <summary>
        /// Method to add "Object" in database by names "Object", "Type" and "Status".
        /// </summary>
        /// <param name="name">Name "Object".</param>
        /// <param name="nameType">Name "Type".</param>
        /// <param name="nameStatus">Name "Status".</param>
        /// <returns>Return the success of the method.</returns>
        public bool AddObject(string name, string nameType, string nameStatus)
        {
            if (!ContainsObjectByName(name) &&
               IsValidObject(name, nameType, nameStatus))
            {
                dbContext.Objects.Add(new EObject()
                {
                    Name = name,
                    Type = GetTypeByName(nameType),
                    Status = GetStatusByName(nameStatus)
                });
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method to remove "Object" in database by name "Object".
        /// </summary>
        /// <param name="name">Name "Object".</param>
        /// <returns>Return the success of the method.</returns>
        public bool RemoveObject(string name)
        {
            if (String.IsNullOrEmpty(name))
                return false;

            EObject eObject = GetObjectByName(name);

            if (eObject != null)
            {
                this.RemoveRelate(name);
                dbContext.Objects.Remove(eObject);
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method to update "Object" in database by name "Object" and new names "Object", "Type", "Status".
        /// </summary>
        /// <param name="oldName">Old name "Object".</param>
        /// <param name="newName">New name "Object".</param>
        /// <param name="nameType">New name "Type".</param>
        /// <param name="nameStatus">New name "Status".</param>
        /// <returns>Return the success of the method.</returns>
        public bool UpdateObject(string oldName, string newName, string nameType, string nameStatus)
        {
            EObject eObject = GetObjectByName(oldName);

            if (oldName != newName && GetObjectByName(newName) != null)
                return false;

            if (IsValidObject(newName, nameType, nameStatus))
            {
                eObject.Name = newName;
                eObject.Type = GetTypeByName(nameType);
                eObject.Status = GetStatusByName(nameStatus);

                dbContext.Objects.Update(eObject);
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method to find and get "Object" in database by name.
        /// </summary>
        /// <param name="name">Name to search for "Object".</param>
        /// <returns>Returned "Object" if finded by name or returned "null".</returns>
        private EObject GetObjectByName(string name)
        {
            var temp = from eObject in dbContext.Objects
                      where eObject.Name == name
                      select eObject;

            if (temp.Count<EObject>() > 0)
                return (EObject)temp.First();

            return null;
        }

        /// <summary>
        /// Method check database to contant "Object" by name.
        /// </summary>
        /// <param name="name">Name to search for "Object".</param>
        /// <returns>Return the success of the method.</returns>
        public bool ContainsObjectByName(string name)
        {
            foreach (EObject eObject in dbContext.Objects)
                if (eObject.Name == name)
                    return true;

            return false;
        }

        /// <summary>
        /// Method to get list all names "Object" in database.
        /// </summary>
        /// <returns>List "string" contains names "Object".</returns>
        public List<string> GetListNamesOfObject()
        {
            var temp = from eObject in dbContext.Objects
                       select eObject.Name;

            return temp.ToList<string>();
        }

        /// <summary>
        /// Check properties "Object" for modify database.
        /// </summary>
        /// <param name="name">Name "Object".</param>
        /// <param name="nameType">Name "Type".</param>
        /// <param name="nameStatus">Name "Status".</param>
        /// <returns>
        /// "true" is correct properties.
        /// "false" is not correct properties.
        /// </returns>
        private bool IsValidObject(string name, string nameType, string nameStatus)
        {
            if (String.IsNullOrEmpty(name) ||
                String.IsNullOrWhiteSpace(name) ||
                GetTypeByName(nameType) == null ||
                GetStatusByName(nameStatus) == null)
                    return false;

            return true;
        }

        #endregion

        #region Statuses

        /// <summary>
        /// Method to add "Status" in database by name "Status".
        /// </summary>
        /// <param name="name">Name "Status".</param>
        /// <returns>Return the success of the method.</returns>
        public bool AddStatus(string name)
        {
            if (IsValidStatus(name))
            {
                dbContext.Statuses.Add(new EStatus() { Name = name });
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method to remove "Status" in database by name "Status".
        /// </summary>
        /// <param name="name">Name "Status".</param>
        /// <returns>Return the success of the method.</returns>
        public bool RemoveStatus(string name)
        {
            if (String.IsNullOrEmpty(name))
                return false;

            EStatus eStatus = GetStatusByName(name);

            if (eStatus != null)
            {
                var temp = from eObject in dbContext.Objects
                           where eObject.Status.Name == name
                           select eObject.Name;

                foreach (string tempName in temp)
                    this.RemoveObject(tempName);

                dbContext.Statuses.Remove(eStatus);
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method to update "Status" in database by name "Status" and new name "Status".
        /// </summary>
        /// <param name="oldName">Old name "Status".</param>
        /// <param name="newName">New name "Status".</param>
        /// <returns>Return the success of the method.</returns>
        public bool UpdateStatus(string oldName, string newName)
        {
            EStatus eStatus = GetStatusByName(oldName);

            if (eStatus != null &&
                IsValidStatus(newName))
            {
                eStatus.Name = newName;
                dbContext.Statuses.Update(eStatus);
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check properties "Status" for modify database.
        /// </summary>
        /// <param name="name">Name "Status".</param>
        /// <returns>
        /// "true" is correct properties.
        /// "false" is not correct properties.
        /// </returns>
        private bool IsValidStatus(string name)
        {
            if (String.IsNullOrEmpty(name) ||
                String.IsNullOrWhiteSpace(name) ||
                GetStatusByName(name) != null)
                    return false;

            return true;
        }

        /// <summary>
        /// Method to find and get "Status" in database by name.
        /// </summary>
        /// <param name="name">Name "Status".</param>
        /// <returns>Returned "Status" if finded by name or returned "null".</returns>
        private EStatus GetStatusByName(string name)
        {
            var temp = from eStatus in dbContext.Statuses
                      where eStatus.Name == name
                      select eStatus;

            if (temp.Count<EStatus>() > 0)
                return (EStatus)temp.First();

            return null;
        }

        /// <summary>
        /// Method check database to contant "Status" by name.
        /// </summary>
        /// <param name="name">Name to search for "Status".</param>
        /// <returns>Return the success of the method.</returns>
        public bool ContainsStatusByName(string name)
        {
            foreach (EStatus eStatus in dbContext.Statuses)
                if (eStatus.Name == name)
                    return true;

            return false;
        }

        /// <summary>
        /// Method to get list all names "Status" in database.
        /// </summary>
        /// <returns>List "string" contains names "Status".</returns>
        public List<string> GetListNamesOfStatus()
        {
            var temp = from eStatus in dbContext.Statuses
                       select eStatus.Name;

            return temp.ToList<string>();
        }

        #endregion

        #region Types

        /// <summary>
        /// Method to add "Type" in database by name "Type".
        /// </summary>
        /// <param name="name">Name "Type".</param>
        /// <returns>Return the success of the method.</returns>
        public bool AddType(string name)
        {
            if (IsValidType(name))
            {
                dbContext.Types.Add(new EType() { Name = name });
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method to remove "Type" in database by name "Type".
        /// </summary>
        /// <param name="name">Name "Type".</param>
        /// <returns>Return the success of the method.</returns>
        public bool RemoveType(string name)
        {
            if (String.IsNullOrEmpty(name))
                return false;

            EType eType = GetTypeByName(name);

            if (eType != null)
            {
                var temp = from eObject in dbContext.Objects
                           where eObject.Type.Name == name
                           select eObject.Name;

                foreach (string tempName in temp)
                    this.RemoveObject(tempName);

                dbContext.Types.Remove(eType);
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method to update "Type" in database by name "Type" and new name "Type".
        /// </summary>
        /// <param name="oldName">Old name "Type".</param>
        /// <param name="newName">New name "Type".</param>
        /// <returns>Return the success of the method.</returns>
        public bool UpdateType(string oldName, string newName)
        {
            EType eType = GetTypeByName(oldName);

            if (eType != null &&
                IsValidType(newName))
            {
                eType.Name = newName;
                dbContext.Types.Update(eType);
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check properties "Type" for modify database.
        /// </summary>
        /// <param name="name">Name "Type".</param>
        /// <returns>
        /// "true" is correct properties.
        /// "false" is not correct properties.
        /// </returns>
        private bool IsValidType(string name)
        {
            if (String.IsNullOrEmpty(name) ||
                String.IsNullOrWhiteSpace(name) || 
                GetTypeByName(name) != null)
                    return false;

            return true;
        }

        /// <summary>
        /// Method to find and get "Type" in database by name.
        /// </summary>
        /// <param name="name">Name "Type".</param>
        /// <returns>Returned "Type" if finded by name or returned "null".</returns>
        private EType GetTypeByName(string name)
        {
            var tmp = from eType in dbContext.Types
                      where eType.Name == name
                      select eType;

            if (tmp.Count<EType>() > 0)
                return (EType)tmp.First();

            return null;
        }

        /// <summary>
        /// Method check database to contant "Type" by name.
        /// </summary>
        /// <param name="name">Name to search for "Type".</param>
        /// <returns>Return the success of the method.</returns>
        public bool ContainsTypeByName(string name)
        {
            foreach (EType eType in dbContext.Types)
                if (eType.Name == name)
                    return true;

            return false;
        }

        /// <summary>
        /// Method to get list all names "Type" in database.
        /// </summary>
        /// <returns>List "string" contains names "Type".</returns>
        public List<string> GetListNamesOfType()
        {
            var temp = from eType in dbContext.Types
                       select eType.Name;

            return temp.ToList<string>();
        }

        #endregion

        #region Relates

        /// <summary>
        /// Method to add "Relate" in database by names child "Object" and parent "Object".
        /// </summary>
        /// <param name="childNameObject">Child name "Object".</param>
        /// <param name="parentNameObject">Parent name "Object".</param>
        /// <returns>Return the success of the method.</returns>
        public bool AddRelate(string childNameObject, string parentNameObject)
        {
            if (IsValidRelate(childNameObject, parentNameObject) &&
                !ContainsRelateByChildObjectName(childNameObject))
            {
                EObject temp;

                if (String.IsNullOrEmpty(parentNameObject))
                    temp = null;
                else
                    temp = GetObjectByName(parentNameObject);

                dbContext.Relates.Add(new ERelate()
                {
                    ObjectChild = GetObjectByName(childNameObject),
                    ObjectParent = temp
                });
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method to remove "Relate" in database by name child "Object".
        /// </summary>
        /// <param name="childNameObject">Child name "Object".</param>
        /// <returns>Return the success of the method.</returns>
        public bool RemoveRelate(string childNameObject)
        {

            if (String.IsNullOrEmpty(childNameObject))
                return false;

            ERelate eRelate = GetRelateByChildObjectName(childNameObject);

            if (eRelate != null)
            {
                IReadOnlyList<EObject> temp = GetListChildObjectsByParentName(eRelate.ObjectChild.Name);
                foreach (EObject eObject in temp)
                {
                    ERelate toRootRelate = GetRelateByChildObjectName(eObject.Name);
                    toRootRelate.ObjectParent = null;
                    dbContext.Relates.Update(toRootRelate);
                }

                dbContext.Relates.Remove(eRelate);
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method to update "Relate" in database by name child "Object" and new parent name "Object".
        /// </summary>
        /// <param name="childNameObject">Child name "Object".</param>
        /// <param name="newParentNameObject">New parent name "Object".</param>
        /// <returns>Return the success of the method.</returns>
        public bool UpdateRelate(string childNameObject, string newParentNameObject)
        {
            if (IsValidRelate(childNameObject, newParentNameObject) &&
                ContainsRelateByChildObjectName(childNameObject) && 
                !IsNewParentAsChild(childNameObject, newParentNameObject))
            {
                ERelate eRelate = GetRelateByChildObjectName(childNameObject);

                if (String.IsNullOrEmpty(newParentNameObject))
                    eRelate.ObjectParent = null;
                else
                    eRelate.ObjectParent = GetObjectByName(newParentNameObject);

                dbContext.Relates.Update(eRelate);
                dbContext.SaveChanges();
                this.UpdateMyTrees();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Method to find and get "Relate" in database by child name "Object".
        /// </summary>
        /// <param name="name">Child name "Object".</param>
        /// <returns>Returned "Relate" if finded by child name "Object" or returned "null".</returns>
        private ERelate GetRelateByChildObjectName(string name)
        {
            if (String.IsNullOrEmpty(name))
                return null;

            var temp = from eRelate in dbContext.Relates
                       where eRelate.ObjectChild.Name == name
                       select eRelate;

            if (temp.Count<ERelate>() > 0)
                return (ERelate)temp.First();

            return null;
        }

        /// <summary>
        /// Method check database to contant "Relate" by child name "Object".
        /// </summary>
        /// <param name="name">Child name "Object".</param>
        /// <returns>Return the success of the method.</returns>
        public bool ContainsRelateByChildObjectName(string name)
        {
            foreach (ERelate eRelate in dbContext.Relates)
                if (eRelate.ObjectChild.Name == name)
                    return true;

            return false;
        }

        /// <summary>
        /// Method check to contain new parent "Object" in child "Object" as child.
        /// When try update "Relate".
        /// </summary>
        /// <param name="childNameObject">Name child "Object".</param>
        /// <param name="newParentNameObject">Name new parent "Object".</param>
        /// <returns>
        /// "true" is new parent "Object" is a child.
        /// "false" is new parent "Object" is not a child.
        /// </returns>
        public bool IsNewParentAsChild(string childNameObject, string newParentNameObject)
        {
            IReadOnlyList<EObject> temp = GetListChildObjectsByParentName(childNameObject);

            foreach(EObject eObject in temp)
            {
                if (eObject.Name == newParentNameObject)
                    return true;

                if (IsNewParentAsChild(eObject.Name, newParentNameObject))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Check properties "Relate" for modify database.
        /// </summary>
        /// <param name="child">Child name "Object".</param>
        /// <param name="parent">Parent name "Object".</param>
        /// <returns>
        /// "true" is correct properties.
        /// "false" is not correct properties.
        /// </returns>
        private bool IsValidRelate(string child, string parent)
        {
            if (!String.IsNullOrEmpty(child))
            {
                if (String.IsNullOrEmpty(parent))
                    return true;
                else if (child != parent &&
                    ContainsRelateByChildObjectName(parent))
                        return true;
            }

            return false;
        }

        #endregion

        //-----------------------------------------------------------------------------

        /// <summary>
        /// Method to get "string" is contains tree names "Object". For to use dialog message or console application.
        /// </summary>
        /// <returns>"string" contains tree names "Object".</returns>
        public string GetTreeNamesToString() =>
            GetTreeNamesToString(String.Empty, 0);

        /// <summary>
        /// Method to recursive entrance to the inner tree.
        /// </summary>
        /// <param name="nameParentObject">Name parent "Object" to recursive entrance.</param>
        /// <param name="_otsup">Level entrance.</param>
        /// <returns>Returned names "Object" of tree on current level recursive entrance.</returns>
        private string GetTreeNamesToString(string nameParentObject, int _otsup)
        {
            string buf = "";

            IReadOnlyList<EObject> ChildsObjects = GetListChildObjectsByParentName(nameParentObject);

            for (int i = 0; i < ChildsObjects.Count; i++)
            {
                for (int j = 0; j < _otsup; j++)
                    buf += "\t";

                buf += ChildsObjects[i].Name;

                int CountNewChildrens = GetListChildObjectsByParentName(ChildsObjects[i].Name).Count;

                if (CountNewChildrens > 0)  // Check, is there an inner tree?
                {
                    buf += "\n";
                    buf += GetTreeNamesToString(ChildsObjects[i].Name, _otsup + 1); // Recursive entrance to the inner tree.
                }

                if (CountNewChildrens == 0)
                    buf += "\n";
            }

            return buf;
        }

        /// <summary>
        /// Method to get list child "Object" by parent name "Object".
        /// </summary>
        /// <param name="name">Name parent "Object".</param>
        /// <returns>List child "Object".</returns>
        private IReadOnlyList<EObject> GetListChildObjectsByParentName(string name)
        {
            if (!String.IsNullOrEmpty(name))
            {
                var temp = from eRelate in dbContext.Relates
                          where eRelate.ObjectParent.Name == name
                          select eRelate.ObjectChild;

                if (temp.Count<EObject>() > 0)
                    return temp.ToList();
            }
            else
            {
                var temp = from eRelate in dbContext.Relates
                          where eRelate.ObjectParent == null
                          select eRelate.ObjectChild;

                if (temp.Count<EObject>() > 0)
                    return temp.ToList();
            }

            return new List<EObject>();
        }

        /// <summary>
        /// Method to get list names "Object" without "Relate".
        /// </summary>
        /// <returns>List names "Object".</returns>
        public List<string> GetNamesOfObjectsWithoutRelate()
        {
            List<string> temp = new List<string>();

            foreach (EObject eObject in dbContext.Objects)
                    if (!ContainsRelateByChildObjectName(eObject.Name))
                        temp.Add(eObject.Name);

            return temp;
        }

        /// <summary>
        /// Method to get name "Type" by name "Object".
        /// </summary>
        /// <param name="name">Name "Object".</param>
        /// <returns>Name "Type".</returns>
        public string GetNameTypeByObjectName(string name)
        {
            var temp = from eObject in dbContext.Objects
                       where eObject.Name == name
                       select eObject.Type.Name;

            if (temp.Count<string>() > 0)
                return temp.First();

            return String.Empty;
        }

        /// <summary>
        /// Method to get name "Status" by name "Object".
        /// </summary>
        /// <param name="name">Name "Object".</param>
        /// <returns>Name "Status".</returns>
        public string GetNameStatusByObjectName(string name)
        {
            var temp = from eObject in dbContext.Objects
                       where eObject.Name == name
                       select eObject.Status.Name;

            if (temp.Count<string>() > 0)
                return temp.First();

            return String.Empty;
        }

        /// <summary>
        /// Method to get name parent "Object" by child "Object" name.
        /// </summary>
        /// <param name="name">Child name "Object".</param>
        /// <returns>Parent name "Object".</returns>
        public string GetNameParentObjectByChildObjectName(string name)
        {
            var temp = from eRelate in dbContext.Relates
                       where eRelate.ObjectChild.Name == name
                       select eRelate.ObjectParent.Name;

            if (temp.Count<string>() > 0)
                return temp.First();

            return String.Empty;
        }

        /// <summary>
        /// Method to get list "MyItem" item contain properties names "Object", "Type" and "Status".
        /// </summary>
        /// <returns>List "MyItem".</returns>
        public List<MyItem> GetListObjectsWithProperties()
        {
            List<MyItem> temp = new List<MyItem>();

            foreach(EObject eObject in dbContext.Objects)
                temp.Add(new MyItem(eObject.Name,
                                    eObject.Type.Name,
                                    eObject.Status.Name));

            return temp;
        }

        //-----------------------------------------------------------------------------

        /// <summary>
        /// Method to update "myTrees" private property.
        /// </summary>
        private void UpdateMyTrees()
        {
            myTrees.Clear();
            foreach(MyTreeItem item in GetMyTree(null))
                myTrees.Add(item);
        }

        /// <summary>
        /// Method to recursive populate collection, contained tree "MyTreeItem".
        /// </summary>
        /// <param name="myTreeItem">Parent item "MyTreeItem"</param>
        /// <returns>
        /// Observable collection "MyTreeItem" in current level entrance.
        /// </returns>
        private ObservableCollection<MyTreeItem> GetMyTree(MyTreeItem myTreeItem)
        {
            ObservableCollection<MyTreeItem> temp = new ObservableCollection<MyTreeItem>();
            IReadOnlyList<EObject> objects;

            if (myTreeItem != null)
                objects = GetListChildObjectsByParentName(myTreeItem.ItemName);
            else
                objects = GetListChildObjectsByParentName(String.Empty);

            foreach (EObject item in objects)
                temp.Add(new MyTreeItem(item.Name, item.Type.Name, item.Status.Name));

            foreach (MyTreeItem item in temp)
                item.TreeItems = GetMyTree(item);

            return temp;
        }

    }

    /// <summary>
    /// A class for storing properties of the "Object" and with a nested collection of the same class.
    /// Use to tree.
    /// </summary>
    public class MyTreeItem
    {
        public ObservableCollection<MyTreeItem> TreeItems { get; internal set; }
        public string ItemName { get; private set; }
        public string ItemType { get; private set; }
        public string ItemStatus { get; private set; }

        /// <summary>
        /// Constructor MyTreeItem.
        /// </summary>
        /// <param name="name">Name "Object"</param>
        /// <param name="type">Name "Type" in "Object"</param>
        /// <param name="status">Name "Status" in "Object"</param>
        public MyTreeItem(string name, string type, string status)
        {
            this.TreeItems = new ObservableCollection<MyTreeItem>();
            this.ItemName = name;
            this.ItemType = type;
            this.ItemStatus = status;
        }
    }

    /// <summary>
    /// A class for storing properties of the "Object".
    /// </summary>
    public class MyItem
    {
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public string ItemStatus { get; set; }

        /// <summary>
        /// Constructor MyItem.
        /// </summary>
        /// <param name="name">Name "Object"</param>
        /// <param name="type">Name "Type" in "Object"</param>
        /// <param name="status">Name "Status" in "Object"</param>
        public MyItem(string name, string type, string status)
        {
            this.ItemName = name;
            this.ItemType = type;
            this.ItemStatus = status;
        }
    }
}
