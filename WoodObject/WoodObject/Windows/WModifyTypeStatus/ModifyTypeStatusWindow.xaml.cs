﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WoodObject.Windows.WModifyTypeStatus
{
    /// <summary>
    /// Interaction logic for ModifyTypeStatusWindow.xaml
    /// </summary>
    public partial class ModifyTypeStatusWindow : Window
    {
        public ModifyTypeStatusWindow(ref MyModel.MyModel model)
        {
            InitializeComponent();
            ModifyTypeStatusVM modifyTypeStatusVM = new ModifyTypeStatusVM(ref model);
            this.DataContext = modifyTypeStatusVM;
        }

        private void Button_Click_Exit(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
