﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DevExpress.Mvvm;
using MyModel;
using System.Windows.Input;

namespace WoodObject.Windows.WModifyTypeStatus
{
    /// <summary>
    /// Class ViewModel for modify types and statuses window.
    /// </summary>
    public class ModifyTypeStatusVM : ViewModelBase
    {
        private readonly MyModel.MyModel model;

        /// <summary>
        /// Constructor ViewModel for modify types and statuses window.
        /// </summary>
        /// <param name="myModel">
        /// Transfer Model without duplication,
        /// so as not to lose changes.
        /// </param>
        public ModifyTypeStatusVM(ref MyModel.MyModel myModel)
        {
            model = myModel;
        }

        private List<string> namesType => model.GetListNamesOfType();
        public List<string> NamesType => namesType;

        private List<string> namesStatus => model.GetListNamesOfStatus();
        public List<string> NamesStatus => namesStatus;

        //--Type--

        private string selectedNameType;
        public string SelectedNameType
        {
            get => selectedNameType;
            set
            {
                selectedNameType = value;
                CurrentNameType = value;
                base.RaisePropertiesChanged(nameof(SelectedNameType));
            }
        }

        private string currentNameType;
        public string CurrentNameType
        {
            get => currentNameType;
            set
            {
                currentNameType = value;
                base.RaisePropertiesChanged(nameof(CurrentNameType));
            }
        }

        private ICommand addTypeCommand;
        public ICommand AddTypeCommand =>
            addTypeCommand ?? (addTypeCommand = new DelegateCommand(delegate ()
            {
                if (!String.IsNullOrEmpty(CurrentNameType))
                {
                    model.AddType(CurrentNameType);
                    base.RaisePropertiesChanged(nameof(NamesType));
                }
            }));

        private ICommand removeTypeCommand;
        public ICommand RemoveTypeCommand =>
            removeTypeCommand ?? (removeTypeCommand = new DelegateCommand(delegate ()
            {
                if (!String.IsNullOrEmpty(CurrentNameType))
                {
                    model.RemoveType(CurrentNameType);
                    SelectedNameType = String.Empty;
                    base.RaisePropertiesChanged(nameof(NamesType));
                }
            }));

        private ICommand updateTyCommand;
        public ICommand UpdateTypeCommand =>
            updateTyCommand ?? (updateTyCommand = new DelegateCommand(delegate ()
            {
                if (!String.IsNullOrEmpty(CurrentNameType) &&
                    !String.IsNullOrEmpty(SelectedNameType))
                {
                    model.UpdateType(SelectedNameType, CurrentNameType);
                    base.RaisePropertiesChanged(nameof(NamesType));
                }
            }));

        //--Status--

        private string selectedNameStatus;
        public string SelectedNameStatus
        {
            get => selectedNameStatus;
            set
            {
                selectedNameStatus = value;
                CurrentNameStatus = value;
                base.RaisePropertiesChanged(nameof(SelectedNameStatus));
            }
        }

        private string currentNameStatus;
        public string CurrentNameStatus
        {
            get => currentNameStatus;
            set
            {
                currentNameStatus = value;
                base.RaisePropertiesChanged(nameof(CurrentNameStatus));
            }
        }

        private ICommand addStatusCommand;
        public ICommand AddStatusCommand =>
            addStatusCommand ?? (addStatusCommand = new DelegateCommand(delegate ()
            {
                if (!String.IsNullOrEmpty(CurrentNameStatus))
                {
                    model.AddStatus(CurrentNameStatus);
                    base.RaisePropertiesChanged(nameof(NamesStatus));
                }
            }));

        private ICommand removeStatusCommand;
        public ICommand RemoveStatusCommand =>
            removeStatusCommand ?? (removeStatusCommand = new DelegateCommand(delegate ()
            {
                if (!String.IsNullOrEmpty(CurrentNameStatus))
                {
                    model.RemoveStatus(CurrentNameStatus);
                    SelectedNameStatus = String.Empty;
                    base.RaisePropertiesChanged(nameof(NamesStatus));
                }
            }));

        private ICommand updateStatusTyCommand;
        public ICommand UpdateStatusCommand =>
            updateStatusTyCommand ?? (updateStatusTyCommand = new DelegateCommand(delegate ()
            {
                if (!String.IsNullOrEmpty(CurrentNameStatus) &&
                    !String.IsNullOrEmpty(SelectedNameStatus))
                {
                    model.UpdateStatus(SelectedNameStatus, CurrentNameStatus);
                    base.RaisePropertiesChanged(nameof(NamesStatus));
                }
            }));
    }
}
