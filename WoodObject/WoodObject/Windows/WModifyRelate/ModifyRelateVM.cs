﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyModel;
using DevExpress.Mvvm;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace WoodObject.Windows.WModifyRelate
{
    /// <summary>
    /// Class ViewModel for modify relates window.
    /// </summary>
    public class ModifyRelateVM : ViewModelBase
    {
        private readonly MyModel.MyModel model;

        /// <summary>
        /// Constructor ViewModel for modify relates window.
        /// </summary>
        /// <param name="myModel">
        /// Transfer Model without duplication,
        /// so as not to lose changes.
        /// </param>
        public ModifyRelateVM(ref MyModel.MyModel myModel)
        {
            model = myModel;
        }

        //--Properties--

        public ObservableCollection<MyTreeItem> MyTrees => model.MyTrees;

        public List<string> ListObjects => model.GetListNamesOfObject();
        public List<string> ListNewObjects => model.GetNamesOfObjectsWithoutRelate();

        private MyTreeItem selectedMyTree;
        public MyTreeItem SelectedMyTree
        {
            get => selectedMyTree;
            set
            {
                selectedMyTree = value;
                if (value != null)
                {
                    SelectedObject = value.ItemName;
                    SelectedParentObject = model.GetNameParentObjectByChildObjectName(value.ItemName);
                }

                base.RaisePropertiesChanged(nameof(SelectedMyTree));
            }
        }

        private string selectedObject;
        public string SelectedObject
        {
            get => selectedObject;
            set
            {
                if (!String.IsNullOrEmpty(value))
                    selectedObject = value;
                else
                    selectedObject = String.Empty;
                
                base.RaisePropertiesChanged(nameof(SelectedObject));
            }
        }

        private string selectedParentObject;
        public string SelectedParentObject
        {
            get => selectedParentObject;
            set
            {
                if (!String.IsNullOrEmpty(value))
                    selectedParentObject = value;
                else
                    selectedParentObject = String.Empty;

                base.RaisePropertiesChanged(nameof(SelectedParentObject));
            }
        }

        private string selectedNewObject;
        public string SelectedNewObject
        {
            get => selectedNewObject;
            set
            {
                if (!String.IsNullOrEmpty(value))
                    selectedNewObject = value;
                else
                    selectedNewObject = String.Empty;

                base.RaisePropertiesChanged(nameof(SelectedNewObject));
            }
        }

        //--Commands--

        private ICommand addRelateCommand;
        public ICommand AddRelateCommand =>
            addRelateCommand ?? (addRelateCommand = new DelegateCommand(delegate ()
            {
                if (!String.IsNullOrEmpty(SelectedNewObject))
                {
                    model.AddRelate(SelectedNewObject, SelectedParentObject);
                    base.RaisePropertiesChanged(nameof(MyTrees), nameof(ListNewObjects));
                }
            }));

        private ICommand removeRelateCommand;
        public ICommand RemoveRelateCommand =>
            removeRelateCommand ?? (removeRelateCommand = new DelegateCommand(delegate ()
            {
                if (!String.IsNullOrEmpty(SelectedObject))
                {
                    model.RemoveRelate(SelectedObject);
                    SelectedObject = null;
                    base.RaisePropertiesChanged(nameof(MyTrees), nameof(ListNewObjects));
                }
            }));

        private ICommand updateRelateCommand;
        public ICommand UpdateRelateCommand =>
            updateRelateCommand ?? (updateRelateCommand = new DelegateCommand(delegate ()
            {
                if (!String.IsNullOrEmpty(SelectedObject))
                {
                    model.UpdateRelate(SelectedObject, SelectedParentObject);
                    base.RaisePropertiesChanged(nameof(MyTrees), nameof(ListNewObjects));
                }
            }));

        private ICommand changeToRootCommand;
        public ICommand ChangeToRootCommand =>
            changeToRootCommand ?? (changeToRootCommand = new DelegateCommand(delegate ()
            {
                SelectedParentObject = String.Empty;
            }));
    }
}
