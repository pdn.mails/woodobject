﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MyModel;

namespace WoodObject.Windows.WModifyRelate
{
    /// <summary>
    /// Interaction logic for ModifyRelateWindow.xaml
    /// </summary>
    public partial class ModifyRelateWindow : Window
    {
        private ModifyRelateVM modifyRelateVM;

        public ModifyRelateWindow(ref MyModel.MyModel model)
        {
            InitializeComponent();
            modifyRelateVM = new ModifyRelateVM(ref model);
            this.DataContext = modifyRelateVM;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            modifyRelateVM.SelectedMyTree = (MyTreeItem)e.NewValue;
        }
    }
}
