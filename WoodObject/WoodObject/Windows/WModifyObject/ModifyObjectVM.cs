﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DevExpress.Mvvm;
using System.Windows.Input;
using MyModel;

namespace WoodObject.Windows.WModifyObject
{
    /// <summary>
    /// Class ViewModel for modify objects window.
    /// </summary>
    public class ModifyObjectVM : ViewModelBase
    {
        private readonly MyModel.MyModel model;

        /// <summary>
        /// Constructor ViewModel for modify objects window.
        /// </summary>
        /// <param name="myModel">
        /// Transfer Model without duplication,
        /// so as not to lose changes.
        /// </param>
        public ModifyObjectVM(ref MyModel.MyModel myModel)
        {
            model = myModel;
        }

        //--Properties--

        private List<MyItem> listObjects => model.GetListObjectsWithProperties();
        public List<MyItem> ListObjects => listObjects;

        private List<string> listType => model.GetListNamesOfType();
        public List<string> ListType => listType;

        private List<string> listStatus => model.GetListNamesOfStatus();
        public List<string> ListStatus => listStatus;

        private MyItem selectedObject;
        public MyItem SelectedObject 
        {
            get => selectedObject;
            set
            {
                selectedObject = value;

                if (value != null)
                {
                    SelectedObjectName = value.ItemName;
                    SelectedType = value.ItemType;
                    SelectedStatus = value.ItemStatus;
                }
                else
                {
                    SelectedObjectName = String.Empty;
                    SelectedType = String.Empty;
                    SelectedStatus = String.Empty;
                }
                base.RaisePropertiesChanged(nameof(SelectedObject));
            }
        }

        private string selectedObjectName;
        public string SelectedObjectName
        {
            get => selectedObjectName;
            set
            {
                selectedObjectName = value;
                base.RaisePropertiesChanged(nameof(SelectedObjectName));
            }
        }

        private string selectedType;
        public string SelectedType
        {
            get => selectedType;
            set
            {
                selectedType = value;
                base.RaisePropertiesChanged(nameof(SelectedType));
            }
        }

        private string selectedStatus;
        public string SelectedStatus
        {
            get => selectedStatus;
            set
            {
                selectedStatus = value;
                base.RaisePropertiesChanged(nameof(SelectedStatus));
            }
        }

        //--Commands--

        private ICommand addObjectCommand;
        public ICommand AddObjectCommand =>
            addObjectCommand ?? (addObjectCommand = new DelegateCommand(delegate ()
            {
                if (!String.IsNullOrEmpty(SelectedObjectName) &&
                    !String.IsNullOrEmpty(SelectedType) &&
                    !String.IsNullOrEmpty(SelectedStatus))
                {
                    model.AddObject(SelectedObjectName, SelectedType, SelectedStatus);
                    base.RaisePropertiesChanged(nameof(ListObjects));
                }
            }));

        private ICommand removeObjectCommand;
        public ICommand RemoveObjectCommand =>
            removeObjectCommand ?? (removeObjectCommand = new DelegateCommand(delegate ()
            {
                if (SelectedObject != null &&
                !String.IsNullOrEmpty(SelectedObject.ItemName))
                {
                    model.RemoveObject(SelectedObject.ItemName);
                    SelectedObject = ListObjects.FirstOrDefault();
                    base.RaisePropertiesChanged(nameof(ListObjects));
                }
            }));

        private ICommand updateObjectCommand;
        public ICommand UpdateObjectCommand =>
            updateObjectCommand ?? (updateObjectCommand = new DelegateCommand(delegate ()
            {
                if (SelectedObject != null &&
                    !String.IsNullOrEmpty(SelectedObject.ItemName) &&
                    !String.IsNullOrEmpty(SelectedObjectName) &&
                    !String.IsNullOrEmpty(SelectedType) &&
                    !String.IsNullOrEmpty(SelectedStatus))
                {
                    model.UpdateObject(SelectedObject.ItemName, SelectedObjectName, SelectedType, SelectedStatus);
                    SelectedObject = ListObjects.FirstOrDefault();
                    base.RaisePropertiesChanged(nameof(ListObjects));
                }
            }));
    }
}
