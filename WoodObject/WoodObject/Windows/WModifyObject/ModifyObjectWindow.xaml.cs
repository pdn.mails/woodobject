﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WoodObject.Windows.WModifyObject
{
    /// <summary>
    /// Interaction logic for ModifyObjectWindow.xaml
    /// </summary>
    public partial class ModifyObjectWindow : Window
    {
        public ModifyObjectWindow(ref MyModel.MyModel model)
        {
            InitializeComponent();
            ModifyObjectVM modifyObjectVM = new ModifyObjectVM(ref model);
            this.DataContext = modifyObjectVM;
        }

        private void Button_Click_Exit(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
