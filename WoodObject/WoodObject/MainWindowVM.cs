﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyModel;
using DevExpress.Mvvm;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows;

namespace WoodObject
{
    /// <summary>
    /// Class ViewModel for main window.
    /// </summary>
    public class MainWindowVM : ViewModelBase
    {
        private MyModel.MyModel myModel;

        /// <summary>
        /// Constructor ViewModel for main window.
        /// </summary>
        public MainWindowVM()
        {
            this.myModel = new MyModel.MyModel();
        }

        //--Properties--

        public ObservableCollection<MyTreeItem> MyTrees => myModel.MyTrees;

        private string nameObject;
        public string NameObject
        {
            get => nameObject;
            set
            {
                nameObject = value;
                base.RaisePropertiesChanged(nameof(NameObject));
            }
        }

        private string nameStatus;
        public string NameStatus
        {
            get => nameStatus;
            set
            {
                nameStatus = value;
                base.RaisePropertiesChanged(nameof(NameStatus));
            }
        }

        private string nameType;
        public string NameType
        {
            get => nameType;
            set
            {
                nameType = value;
                base.RaisePropertiesChanged(nameof(NameType));
            }
        }

        private MyTreeItem selectedItem;
        public MyTreeItem SelectedItem
        {
            get => selectedItem;
            set
            {
                selectedItem = value;
                if (value != null)
                {
                    NameObject = SelectedItem.ItemName;
                    NameType = SelectedItem.ItemType;
                    NameStatus = SelectedItem.ItemStatus;
                }
                else
                {
                    NameObject = String.Empty;
                    NameType = String.Empty;
                    NameStatus = String.Empty;

                }
                base.RaisePropertiesChanged(nameof(SelectedItem));
            }
        }

        //--Commands--

        private ICommand addDefaultTreeCommand;
        public ICommand AddDefaultTreeCommand =>
            addDefaultTreeCommand ?? (addDefaultTreeCommand = new DelegateCommand(delegate ()
            {
                this.myModel.AddDefaultData();
                base.RaisePropertiesChanged("MyTrees");
            }));

        private ICommand modifyTypeStatusCommand;
        public ICommand ModifyTypeStatusCommand =>
            modifyTypeStatusCommand ?? (modifyTypeStatusCommand = new DelegateCommand(delegate ()
            {
                MyModel.MyModel model = this.myModel;

                Windows.WModifyTypeStatus.ModifyTypeStatusWindow modifyTypeStatusWindow =
                new Windows.WModifyTypeStatus.ModifyTypeStatusWindow(ref model);

                if (modifyTypeStatusWindow.ShowDialog() == true)
                    this.myModel = model;
            }));

        private ICommand modifyObjectCommand;
        public ICommand ModifyObjectCommand =>
            modifyObjectCommand ?? (modifyObjectCommand = new DelegateCommand(delegate ()
            {
                MyModel.MyModel model = this.myModel;

                Windows.WModifyObject.ModifyObjectWindow modifyObjectWindow =
                new Windows.WModifyObject.ModifyObjectWindow(ref model);

                if (modifyObjectWindow.ShowDialog() == true)
                    this.myModel = model;

                base.RaisePropertiesChanged(nameof(MyTrees));
            }));

        private ICommand modifyRelateCommand;
        public ICommand ModifyRelateCommand =>
            modifyRelateCommand ?? (modifyRelateCommand = new DelegateCommand(delegate ()
            {
                MyModel.MyModel model = this.myModel;

                Windows.WModifyRelate.ModifyRelateWindow modifyRelateWindow =
                new Windows.WModifyRelate.ModifyRelateWindow(ref model);

                if (modifyRelateWindow.ShowDialog() == true)
                    this.myModel = model;

                base.RaisePropertiesChanged(nameof(MyTrees));
            }));

        private ICommand showTreeInDialogMessage;
        public ICommand ShowTreeInDialogMessage =>
            showTreeInDialogMessage ?? (showTreeInDialogMessage = new DelegateCommand(delegate ()
            {
                MessageBox.Show(this.myModel.GetTreeNamesToString());
            }));
    }
}
