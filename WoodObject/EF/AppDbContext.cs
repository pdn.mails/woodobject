﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;
using Core;
using System.IO;

namespace EF
{
    /// <summary>
    /// Application database context.
    /// </summary>
    public class AppDbContext : DbContext
    {
        public DbSet<EObject> Objects { get; set; }
        public DbSet<EStatus> Statuses { get; set; }
        public DbSet<EType> Types { get; set; }
        public DbSet<ERelate> Relates { get; set; }

//-----------------------------------------------------------------------------

        public AppDbContext()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var path = new DirectoryInfo(Environment.CurrentDirectory).FullName;

            optionsBuilder.UseSqlite($@"Data Source={path}\main.db;");
        }
    }
}
