﻿using System;

namespace Core
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}
