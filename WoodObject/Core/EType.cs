﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class EType : IEntity
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
