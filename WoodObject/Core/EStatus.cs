﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class EStatus : IEntity
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
