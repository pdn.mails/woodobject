﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core
{
    public class ERelate : IEntity
    {
        public long Id { get; set; }

        public long? ObjectParentId { get; set; }
        [ForeignKey("ObjectParentId")]
        public EObject ObjectParent { get; set; } // null

        public long ObjectChildId { get; set; }
        [ForeignKey("ObjectChildId")]
        public EObject ObjectChild { get; set; } // not null
    }
}
