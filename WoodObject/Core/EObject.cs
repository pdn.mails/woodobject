﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Core
{
    public class EObject : IEntity
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long TypeId { get; set; }
        [ForeignKey("TypeId")]
        public EType Type { get; set; }

        public long StatusId { get; set; }
        [ForeignKey("StatusId")]
        public EStatus Status { get; set; }
    }
}
