Task for the project. (Primary).
  
Initial data:
Database includes 4 data tables:
  
States (the table stores a list of all possible states of objects)
Fields:
1) Identifier (type: integer)
2) Name (type: string)
  
Types (the table contains a list of all possible types of objects)
Fields:
1) Identifier (type: integer)
2) name (type: string)
  
Objects (the table stores a list of objects with an indication of their type and
state)
Fields:
1) Identifier (type: integer)
2) Name (type: string)
3) Type Identifier (type: integer)
4) Status Id (type: integer)
  
Links (table of relations between objects. If the value of the identifier of the
parent object is NULL, then it is a top-level object)
Fields:
1) Identifier (type: integer)
2) Parent object identifier (type: integer, NULL allowed)
3) Object ID of the child (type: integer)
  
  
Task:
Write scripts to create tables, create tables, fill them with a small amount of 
test data needed to demonstrate the work.
Write an application to build a tree of objects. The text of the nodes must 
contain the name of the object, its type and state.
  
Requirements:
Database scripts: Transact SQL, MS SQL DBMS 2000 or higher, possibly Express
Or use ORM (EF, Linq, NHibernate)
  
Appendix: C #, FW not lower than 2.0.
Application Type: Windows Forms

---

Original text.
---

Задание для проекта. (Начальное).  
  
Исходные данные:  
База данных включает 4 таблицы данных:
  
Состояния (в таблице хранится список всех возможных состояний объектов)  
Поля: 	
1) Идентификатор (тип: целое число)  
2) Наименование (тип: строка)  
  
Типы (в таблице хранится список всех возможных типов объектов)  
Поля: 
1) Идентификатор (тип: целое число)  
2) наименование (тип: строка)  
  
Объекты (в таблице храниться список объектов с указанием их типа и состояния)  
Поля:  
1) Идентификатор (тип: целое число)  
2) Наименование (тип: строка)  
3) Идентификатор Типа (тип: целое число)  
4) Идентификатор Состояния (тип: целое число)  
  
Связи (таблица связей между объектами. Если значение идентификатора объекта 
родителя равно NULL, то это объект верхнего уровня)  
Поля:  
1) Идентификатор (тип: целое число)  
2) Идентификатор объекта родителя (тип: целое число, допускается NULL)  
3) Идентификатор объекта потомка (тип: целое число)  
  
  
Задача:  
Написать скрипты создания таблиц, создать таблицы, заполнить их небольшим 
количеством тестовых данных, необходимых для демонстрации работы.  
Написать приложение построения дерева объектов. Текст узлов должен содержать 
наименование объекта, его тип и состояние.  
  
Требования:  
Скрипты работы с базой данных: Transact SQL, СУБД MS SQL не ниже 2000, возможно 
Express  
Либо использование ORM (EF, Linq, NHibernate)  
  
Приложение: С#, FW не ниже 2.0.  
Тип приложения: Windows Forms  
  